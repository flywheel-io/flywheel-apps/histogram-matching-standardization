FROM flywheel/fw-gear-slicer-base:v4.11.20210226

COPY pyproject.toml poetry.lock $FLYWHEEL/
RUN poetry env use /root/miniconda3/bin/python
RUN poetry install --no-dev

COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_intensity_standardization $FLYWHEEL/fw_gear_intensity_standardization
RUN chmod a+x $FLYWHEEL/run.py

ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]
