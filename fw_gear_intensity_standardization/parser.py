"""Parser module to parse gear config.json."""
from typing import Tuple

from flywheel_gear_toolkit import GearToolkitContext


def parse_config(
    gear_context: GearToolkitContext,
) -> Tuple[str, float, int, float, str]:
    """Parses the config info.
    Args:
        gear_context: Context.

    Returns:
        src_image: A nifti file. Source image to be filtered.
        ref_image: A nifti file. Reference image to be matched to.
        hist_levels: Histogram levels.
        match_points: Matched points.
        thresh: Threshold at mean.
        output_dir: Directory of matched output nifti file.
    """

    hist_levels = gear_context.config.get("hist_levels")
    match_points = gear_context.config.get("match_points")
    thresh = gear_context.config.get("thresh")
    output_dir = gear_context.output_dir

    src_image = gear_context.get_input_path("nifti-input-src")
    ref_image = gear_context.get_input_path("nifti-input-ref")

    return src_image, ref_image, hist_levels, match_points, thresh, output_dir
