# Release notes

## 0.2.0
__Enhancements__:
* Updating the output file name with the suffix "_matched".

## 0.1.0

__Documentation__:

* Add `docs`folder
* Add `docs/release_notes.md`

Notes: Categories used in release notes should match one of the following:

* Fixes
* Enhancements
* Documentation
* Maintenance
