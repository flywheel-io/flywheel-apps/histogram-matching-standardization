#!/usr/bin/env python
"""The run script"""
import logging
import sys

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_intensity_standardization.main import run
from fw_gear_intensity_standardization.parser import parse_config

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config and run"""

    src_image, ref_image, hist_levels, match_points, thresh, output_dir = parse_config(
        context
    )
    e_code = run(src_image, ref_image, hist_levels, match_points, thresh, output_dir)
    sys.exit(e_code)


if __name__ == "__main__":

    with GearToolkitContext() as gear_context:

        gear_context.init_logging()
        main(gear_context)
