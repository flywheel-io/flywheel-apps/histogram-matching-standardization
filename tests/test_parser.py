"""Module to test parser.py"""
from unittest.mock import MagicMock

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_intensity_standardization.parser import parse_config


def test_parse_config(tmp_path):
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = {"hist_levels": 256, "match_points": 20, "thresh": "false"}
    gear_context.output_dir = tmp_path
    gear_context.get_input_path.side_effect = (
        tmp_path / "test_image_src.nii.gz",
        tmp_path / "test_image_ref.nii.gz",
    )

    (src_image, ref_image, hist_levels, match_points, thresh, out_dir) = parse_config(
        gear_context
    )
    assert hist_levels == 256
    assert match_points == 20
    assert thresh == "false"
    assert out_dir == tmp_path

    assert gear_context.get_input_path.call_args_list[0].args[0] == "nifti-input-src"
    assert gear_context.get_input_path.call_args_list[1].args[0] == "nifti-input-ref"
    assert src_image == (tmp_path / "test_image_src.nii.gz")
    assert ref_image == (tmp_path / "test_image_ref.nii.gz")
